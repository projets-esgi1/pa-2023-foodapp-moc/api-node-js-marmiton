import { ApplicationConfig, CookupApiApplication } from './application';
export * from './application';
export declare function main(options?: ApplicationConfig): Promise<CookupApiApplication>;
