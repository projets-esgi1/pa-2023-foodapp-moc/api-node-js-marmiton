"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
tslib_1.__exportStar(require("./ping.controller"), exports);
tslib_1.__exportStar(require("./user.controller"), exports);
tslib_1.__exportStar(require("./recipe.controller"), exports);
tslib_1.__exportStar(require("./messages.controller"), exports);
tslib_1.__exportStar(require("./award.controller"), exports);
tslib_1.__exportStar(require("./competition.controller"), exports);
tslib_1.__exportStar(require("./performance.controller"), exports);
tslib_1.__exportStar(require("./league.controller"), exports);
tslib_1.__exportStar(require("./challenge.controller"), exports);
tslib_1.__exportStar(require("./forum.controller"), exports);
tslib_1.__exportStar(require("./pointlevel.controller"), exports);
tslib_1.__exportStar(require("./conversation.controller"), exports);
tslib_1.__exportStar(require("./messages-user.controller"), exports);
tslib_1.__exportStar(require("./video.controller"), exports);
tslib_1.__exportStar(require("./photo.controller"), exports);
//# sourceMappingURL=index.js.map